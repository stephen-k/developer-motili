import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './foot.css';

export class Foot extends Component {
  constructor() {
    super();

    this.joinArray = this.joinArray.bind(this);
    this.formatArray = this.formatArray.bind(this);
    this.formatString = this.formatString.bind(this);
  }

  joinArray(arr) {
    return(
      <p className="big-text">
        <i className="fa fa-quote-left" aria-hidden="true"></i> {arr.join(", ")}
      </p>
    )
  }

  formatArray(misc, key, i) {
    let arr = misc[key];
    return <p key={i} className="footer-text">{arr.join(", ")}</p>;
  }

  formatString(key, i) {
    let oKey = key.charAt(0).toUpperCase() + key.slice(1, key.length);
    return <span key={i} className="footer-tag">{oKey}</span>
  }

  render() {
    if (this.props) {
      let props = this.props;
      let misc = props.misc;
      let other = props.other;
      return(
        <footer className="footer">
          <div className="footer-box">
            <h5><i className="fa fa-heart" aria-hidden="true"></i> Benefits</h5>
            {
              Object.keys(misc).map((key, i) => {
                return(
                  key === "freestuff" ?
                    this.formatArray(misc, key, i) : this.formatString(key, i)
                )
              })
            }
          </div>
          <div className="footer-quote">
            {this.joinArray(other)}
          </div>
        </footer>
      )
    }
    return null;
  }
}

Foot.propTypes = {
  misc: PropTypes.object,
  other: PropTypes.array
}

export default Foot;
