import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './technologies.css';

const formatObj = (stack, key) => {
  let obj = stack[key];
  let zObj = obj["oneof"]
  return <Labels stack={zObj} />
}

const formatString = (stack, key) => {
  let text = stack[key];
  return <li className="inner-label-item">{text}</li>
}

const Labels = props => {
  if (props) {
    let stack = props.stack;
    return(
      <ul className="labels-list">
        {
          Object.keys(stack).map((key, i) => {
            return(
              <li key={i} className="label-item clearfix">
                <span className="label-item-text">{key}</span>
                <ul className="inner-labels-list">
                  {
                    key === "testing" || key === "framework" ?
                      formatObj(stack, key) : formatString(stack, key)
                  }
                </ul>
              </li>
            )
          })
        }
      </ul>
    )
  }
  return null;
}

export class Technologies extends Component {
  render() {
    if (this.props) {
      let props = this.props;
      let tech = props.tech;
      let junior = tech.junior;
      let seasoned = tech.seasoned;
      return(
        <div className="k-container-wrap clearfix">
          <div className="k-container">
            <div className="k-sub-container">
              <h5>Seasoned</h5>
              <Labels stack={seasoned} />
            </div>
          </div>
          <div className="k-container">
            <div className="k-sub-container">
              <h5>Junior</h5>
              <Labels stack={junior} />
            </div>
          </div>
        </div>
      )
    }
    return null;
  }
}

Technologies.propTypes = {
  tech: PropTypes.object
}

export default Technologies;
