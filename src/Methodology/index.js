import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './methodology.css';

export class Methodology extends Component {
  constructor() {
    super();

    this.formatTag = this.formatTag.bind(this);
  }

  formatTag(key, obj) {
    let tag;
    if (typeof obj[key] === "boolean") {
      return tag = key;
    } else {
      return tag = `${key} - ${obj[key]}`;
    }
    return tag;
  }

  render() {
    if (this.props) {
      let props = this.props;
      let methodologies = props.methods;
      return(
        <div className="m-container clearfix">
          <div className="img-roll">
            <div className="lightbox"></div>
          </div>
          <div className="c-box">
            {
              Object.keys(methodologies).map((key, i) => {
                return <span key={i} className="s-tag">{this.formatTag(key, methodologies)}</span>
              })
            }
          </div>
        </div>
      )
    }
    return null;
  }
}

Methodology.propTypes = {
  methods: PropTypes.object
}

export default Methodology;
