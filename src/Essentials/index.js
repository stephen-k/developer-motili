import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './essentials.css';

export class Essentials extends Component {
  constructor() {
    super();

    this.formatDate = this.formatDate.bind(this);
  }

  formatDate(date) {
    var nDate = new Date(date);
    return nDate.toLocaleDateString("EN-us");
  }

  render() {
    if (this.props) {
      let essentials = this.props.essentials,
      locations = essentials.locations,
      employment = essentials.employment,
      experienceJr = essentials.experience[0],
      experienceSr = essentials.experience[1],
      startdate = essentials.startdate,
      companysize = essentials.companysize,
      teamsizeMin = essentials.teamsize.min,
      teamsizeMax = essentials.teamsize.max;
      return(
        <div className="essentials">
          <ul className="ul-items">
            <li className="li-item li-item-head">
              <i className="fa fa-map-pin s-icon" aria-hidden="true"></i>
              <span className="s-item">{locations}, CO</span>
            </li>
            <li className="li-item">
              <i className="fa fa-handshake-o s-icon" aria-hidden="true"></i>
              <span className="s-item">{employment}</span>
            </li>
            <li className="li-item">
              <i className="fa fa-code s-icon" aria-hidden="true"></i>
              <span className="s-item">{experienceJr} &amp; {experienceSr}</span>
            </li>
            <li className="li-item">
              <i className="fa fa-calendar s-icon" aria-hidden="true"></i>
              <span className="s-item">{this.formatDate(startdate)} Start</span>
            </li>
            <li className="li-item">
              <i className="fa fa-building-o s-icon" aria-hidden="true"></i>
              <span className="s-item">{companysize} People</span>
            </li>
            <li className="li-item">
              <i className="fa fa-users s-icon" aria-hidden="true"></i>
              <span className="s-item">{teamsizeMin} - {teamsizeMax} Per Team</span>
            </li>
          </ul>
        </div>
      )
    }
    return null;
  }
}

Essentials.propTypes = {
  essentials: PropTypes.object
}

export default Essentials;
