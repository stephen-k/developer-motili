import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './job.css';

const getHour = (hour) => {
  let oHour = hour / 100;
  let ampm = 'am';
  if (oHour >= 12) {
    ampm = 'pm';
    oHour -= 12;
  }

  return `${oHour}${ampm}`;
}



const formatHours = (obj, key) => {
  let hours = obj[key];
  let from = hours.from;
  let to = hours.to;

  let hFrom = getHour(from)
  let hTo = getHour(to);

  return(
    <div>
      <span className="j-item-head">{key}</span>
      <span className="j-item">{hFrom}-{hTo}</span>
    </div>
  )
}

const formatArray = (obj, key) => {
  let zArray = obj[key];
  let oItem = zArray[0];
  let sItem = zArray[1];

  return(
    <div>
      <span className="j-item-head">OS</span>
      <span className="j-item">{oItem} or {sItem}</span>
    </div>
  )
}

const formatString = (obj, key, option) => {
  let zString = obj[key];

  return(
    <div>
      <span className="j-item-head">{key}</span>
      <span className="j-item">{zString}{option}</span>
    </div>
  )
}

const Specs = props => {
  if (props) {
    let specs = props.specs;
    return(
      <div className="j-sub-container">
        <h5>Specs</h5>
        <ul className="j-list">
          {
            Object.keys(specs).map((key, i) => {
              return(
                <li key={i} className="j-list-item clearfix">
                  {
                    key === "corehours" ?
                      formatHours(specs, key) : formatString(specs, key)
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
  return null;
}

const Profile = props => {
  if (props) {
    let profile = props.profile;
    return(
      <div className="j-sub-container">
        <h5>Profile</h5>
        <ul className="j-list">
          {
            Object.keys(profile).map((key, i) => {
              return(
                <li key={i} className="j-list-item clearfix">
                  {formatString(profile, key, "%")}
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
  return null;
}

const Equipment = props => {
  if (props) {
    let equipment = props.equipment;
    return(
      <div className="j-sub-container">
        <h5>Equipment</h5>
        <ul className="j-list">
          {
            Object.keys(equipment).map((key, i) => {
              return(

                <li key={i} className="j-list-item clearfix">
                  {
                    key === "operatingsystem" ?
                      formatArray(equipment, key) : formatString(equipment, key)
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
  return null;
}

export class Job extends Component {
  render() {
    if (this.props) {
      let props = this.props;
      let specs = props.specs;
      let profile = props.profile;
      let equipment = props.equipment;
      return(
        <div className="j-container clearfix">
          <Specs specs={specs} />
          <Profile profile={profile} />
          <Equipment equipment={equipment} />
        </div>
      )
    }
    return null;
  }
}

Job.propTypes = {
  specs: PropTypes.object,
  profile: PropTypes.object,
  equipment: PropTypes.object
}

export default Job;
