import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Head from './Head';
import Methodology from './Methodology';
import Job from './Job';
import Technologies from './Technologies';
import Bonus from './Bonus';
import Foot from './Foot';

import './app.css';

export class App extends Component {
  render() {
    let oJob = window.job || null;
    if (oJob) {
      return(
        <div className="container">
          <Head
            title={oJob.headline}
            essentials={oJob.essentials} />

          <Methodology
            methods={oJob.methodology} />

          <Job
            specs={oJob.specs}
            profile={oJob.profile}
            equipment={oJob.equipment} />

          <Technologies
            tech={oJob.technologies} />

          <Bonus
            bonus={oJob.bonuspoints} />

          <Foot
            other={oJob.other}
            misc={oJob.misc} />
        </div>
      )
    }
    return null;
  }
}

ReactDOM.render(
  <App />,
  document.getElementById("root")
)
