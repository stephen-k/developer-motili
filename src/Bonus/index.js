import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './bonus.css';

export class Bonus extends Component {
  render() {
    if (this.props) {
      let props = this.props;
      let bonus = props.bonus;
      return(
        <div className="b-container">
          <h5>Bonus Points!</h5>
          <ul className="bonus-list">
            {
              Object.keys(bonus).map((key, i) => {
                return(
                  <li key={i} className="bonus-item">
                    <div className="clearfix">
                      <span className="b-key">{key}</span>
                      <span className="b-value">{bonus[key]}</span>
                    </div>
                  </li>
                )
              })
            }
          </ul>
        </div>
      )
    }
    return null;
  }
}

Bonus.propTypes = {
  bonus: PropTypes.object
}

export default Bonus;
