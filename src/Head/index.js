import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Essentials from '../Essentials';

import './head.css';

const Header = props => {
  if (props) {
    let title = props.title;
    return(
        <header className="banner">
          <h1 className="title">{title}</h1>
          <p className="sub-title">@Motili</p>
        </header>
    )
  }
  return null;
}

export class Head extends Component {
  render() {
    if (this.props) {
      let props = this.props;
      let title = props.title;
      let essentials = props.essentials;
      return (
        <div className="head-container clearfix">
          <Header title={title} />
          <Essentials essentials={essentials} />
        </div>
      )
    }
    return null;
  }
}

Head.propTypes = {
  title: PropTypes.string,
  essentials: PropTypes.object
}

export default Head;
