import React from 'react';
import {Methodology} from '../../../src/Methodology';
import Storage from '../Helpers/storage';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {shallow} from 'enzyme';

chai.use(chaiEnzyme());

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Methodology />', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      methods: Storage.Methodology
    };
    wrapper = shallow(<Methodology {...props} />)
  })

  describe('Methodology Component', () => {
    it('should display 17 <span class="s-tag"></span> elements', () => {
      expect(wrapper.find('span.s-tag')).to.have.length(17);
    });
  });
});
