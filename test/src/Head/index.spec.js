import React from 'react';
import {Head} from '../../../src/Head';
import Storage from '../Helpers/storage';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {shallow} from 'enzyme';

chai.use(chaiEnzyme());

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Head />', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      title: Storage.Headline,
      essentials: Storage.Essentials
    };
    wrapper = shallow(<Head {...props} />)
  })

  describe('Head Component', () => {
    it('should display <Header /> Component', () => {
      expect(wrapper.find('Header')).to.exist;
    });
  });

  describe('Head Component', () => {
    it('should display <Essentials /> Component', () => {
      expect(wrapper.find('Essentials')).to.exist;
    });
  });
});
