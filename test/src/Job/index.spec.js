import React from 'react';
import {Job} from '../../../src/Job';
import Storage from '../Helpers/storage';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {shallow} from 'enzyme';

chai.use(chaiEnzyme());

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Job />', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      specs: Storage.Specs,
      profile: Storage.Profile,
      equipment: Storage.Equipment
    };
    wrapper = shallow(<Job {...props} />)
  })

  describe('Job Component', () => {
    it('should display <Specs /> component', () => {
      expect(wrapper.find('Specs')).to.exist;
    });
  });

  describe('Job Component', () => {
    it('should display <Profile /> component', () => {
      expect(wrapper.find('Profile')).to.exist;
    });
  });

  describe('Job Component', () => {
    it('should display <Equipment /> component', () => {
      expect(wrapper.find('Equipment')).to.exist;
    });
  });
});
