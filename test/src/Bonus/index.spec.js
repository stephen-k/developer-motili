import React from 'react';
import {Bonus} from '../../../src/Bonus';
import Storage from '../Helpers/storage';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {shallow} from 'enzyme';

chai.use(chaiEnzyme());

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Bonus />', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      bonus: Storage.Bonus
    };
    wrapper = shallow(<Bonus {...props} />)
  })

  describe('Bonus Component', () => {
    it('should display 4 <li class="bonus-item"></li> elements', () => {
      expect(wrapper.find('li.bonus-item')).to.have.length(4);
    });
  });
});
