const Storage = {};

Storage.Headline = "Developer";

Storage.Essentails = {
  "locations": "denver",
  "employment": "Permanent",
  "experience": ["Junior", "Seasoned"],
  "startdate": (new Date()).getTime(),
  "companysize": "TwentyToFifty",
  "teamsize": { "min": 1, "max": 6 }
}

Storage.Methodology = {
  "codereviews": true,
  "prototyping": true,
  "pairprogramming": true,
  "failfast": true,
  "unittests": true,
  "integrationtests": true,
  "buildserver": "Codeship",
  "staticcodeanalysis": "NotYetChosen",
  "versioncontrol": "BitBucket",
  "issuetracker": "Jira",
  "knowledgerepo": "Confluence",
  "standups": true,
  "qaprotocol": true,
  "freedomovertools": true,
  "onecommandbuild": true,
  "quickstart": true,
  "commitondayone": true,
}

Storage.Specs = {
  "workload": 1.0,
  "workweek": 40,
  "corehours": { from: 800, to: 1700 },
  "schedule": "Flexible",
  "remote": "Negotiable",
  "pto": "Unlimited"
}

Storage.Profile = {
  "newfeatures": 50,
  "clientsupport": 9,
  "documentation": 10,
  "maintenance": 30,
  "meetings": 1,
}

Storage.Equipment = {
  "operatingsystem": ["MacOSX", "CentOS"],
  "computer": "Laptop",
  "monitors": "Negotiable",
}

Storage.Tech = {
  "junior": {
    "css3": "Good",
    "html5": "Good",
    "javascript": "Good",
    "node": "Good",
    "rest": "Good",
    "uiux": "Familiar",
        "design": "Familiar",
        "testing": {
            "oneof": {
                "junit": "Good",
                "mocha": "Good",
                "jasmine": "Good",
                "selenium": "Good",
            }
        },
        "framework": {
            "oneof": {
                "react": "Familiar",
                "vue": "Familiar",
                "angular": "Familiar",
            }
        },
    "boardgames": "Familiar",
  },
  "seasoned": {
    "css3": "Expert",
    "html5": "Expert",
    "javascript": "Expert",
    "node": "Expert",
    "rest": "Expert",
    "uiux": "Good",
        "design": "Good",
        "testing": {
            "oneof": {
                "junit": "Good",
                "mocha": "Good",
                "jasmine": "Good",
                "selenium": "Good",
            }
        },
        "framework": {
            "oneof": {
                "react": "Familiar",
                "vue": "Familiar",
                "angular": "Familiar",
            }
        },
    "boardgames": "Familiar",
  }
}

Storage.Bonus = {
  "devops": "Good",
  "sql": "Good",
  "mobiledevelopment": "Good",
  "quotingbadactionmovies": "Expert"
}

Storage.Other = [
  "we love technology",
  "we solve interesting problems"
]

Storage.Misc = {
  "training": "Informal",
  "teamevents": true,
  "ecopass": true,
  "healthcare": true,
  "dental": true,
  "mobilephone": false,
  "kitchen": true,
  "freestuff": ["coffee (lots)", "beverages (adult and otherwise)", "snacks", "bikeparking"]
}
export default Storage;
