import React from 'react';
import {Technologies} from '../../../src/Technologies';
import Storage from '../Helpers/storage';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {shallow} from 'enzyme';

chai.use(chaiEnzyme());

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Technologies />', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      tech: Storage.Tech
    };
    wrapper = shallow(<Technologies {...props} />)
  })

  describe('Technologies Component', () => {
    it('should display 2 <Labels /> components', () => {
      expect(wrapper.find('Labels')).to.have.length(2);
    });
  });
});
