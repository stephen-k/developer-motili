import React from 'react';
import {Foot} from '../../../src/Foot';
import Storage from '../Helpers/storage';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {shallow} from 'enzyme';

chai.use(chaiEnzyme());

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Foot />', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      misc: Storage.Misc,
      other: Storage.Other
    };
    wrapper = shallow(<Foot {...props} />)
  })

  describe('Foot Component', () => {
    it('should display 7 <span class="footer-tag"></span> elements', () => {
      expect(wrapper.find('span.footer-tag')).to.have.length(7);
    });
  });

  describe('Foot Component', () => {
    it('should display 1 <p class="footer-text"></p> element', () => {
      expect(wrapper.find('p.footer-text')).to.have.length(1);
    });
  });

  describe('Foot Component', () => {
    it('should display 1 <div class="footer-quote"></div> element', () => {
      expect(wrapper.find('div.footer-quote')).to.have.length(1);
    });
  });
});
